<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <id>@appid@.desktop</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0-or-later</project_license>
  <name>Saldo</name>
  <summary>Check your bank accounts</summary>
  <description>
    <p>
      An easy way to access your online banking information. Show your balance and transaction based on FinTS online banking information.
    </p>
    <p>
      Note: This is a frontend for python-fints. Report non working bank access at python-fints github page.
    </p>
	FinTS online banking application designed for Linux smartphones.
  </description>
  <screenshots>
    <screenshot>
      <image>https://gitlab.com/tabos/saldo/-/raw/main/data/screenshots/saldo1.png</image>
      <caption>Lock screen</caption>
    </screenshot>
    <screenshot type="default">
      <image>https://gitlab.com/tabos/saldo/-/raw/main/data/screenshots/saldo2.png</image>
      <caption>Main view</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/tabos/saldo/-/raw/main/data/screenshots/saldo3.png</image>
      <caption>Details view</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/tabos/saldo/-/raw/main/data/screenshots/saldo4.png</image>
      <caption>Search view</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/tabos/saldo/-/raw/main/data/screenshots/saldo5.png</image>
      <caption>Setup assistant</caption>
    </screenshot>  </screenshots>
  <developer_name>Jan-Michael Brummer</developer_name>
  <launchable type="desktop-id">@appid@.desktop</launchable>
  <url type="homepage">@package_url@</url>
  <url type="bugtracker">@package_url_bug@</url>
  <url type="donation">https://www.paypal.com/paypalme/tabos/10/</url>
  <url type="help">https://gitlab.com/tabos/saldo/-/issues/</url>
  <kudos>
    <kudo>ModernToolkit</kudo>
  </kudos>
  <update_contact>jan.brummer@tabos.org</update_contact>
  <translation type="gettext">@appid@</translation>
  <content_rating type="oars-1.1" />
  <releases>
    <release version="0.7.0" date="2023-09-10">
      <description>
        <p>Breaking release: Database must be recreated!</p>
        <p>New Features:</p>
        <ul>
          <li>GNOME Circle UI updates</li>
          <li>Improve database storage</li>
          <li>Restore comdirect logo</li>
          <li>Set upper sales days limit to 100.000</li>
          <li>Updated banking list</li>
        </ul>
      </description>
    </release>
    <release version="0.6.0" date="2022-10-30">
      <description>
        <p>New Features:</p>
        <ul>
          <li>Support for multiple clients</li>
          <li>Option to change safe password</li>
          <li>Using modern UI elements libadwaita</li>
          <li>Support for dark mode</li>
          <li>Improved support for systems without locale</li>
          <li>Updated banking list</li>
        </ul>
      </description>
    </release>
    <release version="0.5.1" date="2022-05-29">
      <description>
        <p>New Features:</p>
        <ul>
          <li>Fix account update</li>
          <li>Client bank code indicator</li>
        </ul>
      </description>
    </release>
    <release version="0.5.0" date="2022-05-27">
      <description>
        <p>New Features:</p>
        <ul>
          <li>Encrypted sqlite storage</li>
          <li>Improved category support</li>
          <li>Notification for new and first time transactions</li>
          <li>UI refactoring</li>
          <li>Run in background option</li>
          <li>New application logo</li>
          <li>Fix random crash during initial setup</li>
        </ul>
      </description>
    </release>
    <release version="0.4.0" date="2021-10-01">
      <description>
        <p>New Features:</p>
        <ul>
          <li>Port to GTK4</li>
          <li>Update server database</li>
          <li>Support for TAN mechanism selection</li>
          <li>PhotoTAN and FlickerCode support</li>
          <li>In-App notification</li>
          <li>Category support</li>
          <li>Search functionality</li>
        </ul>
      </description>
    </release>
    <release version="0.3.0" date="2020-12-23" urgency="medium" />
    <release version="0.2.4" date="2020-10-25" urgency="medium" />
    <release version="0.2.3" date="2020-10-24" urgency="medium" />
    <release version="0.2.2" date="2020-10-23" urgency="medium" />
    <release version="0.2.1" date="2020-10-20" urgency="medium" />
    <release version="0.2.0" date="2020-10-20" urgency="medium" />
  </releases>
  <custom>
    <value key="Purism::form_factor">workstation</value>
    <value key="Purism::form_factor">mobile</value>
  </custom>
</component>
